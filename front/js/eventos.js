function validaLogin() {
    let userTxt = localStorage.getItem("userLogged");
    
    if (!userTxt) {
        window.location="index.html";    
    }

}

function logout() {
    localStorage.removeItem("userLogged");
    window.location="index.html"; 
}

function voltar() {
    window.location = "dashmenu.html";
}

function gerarRelatorioEventos() {
    //event.preventDefault();   // evita o comportamento padrão do form. não envia os dados do form.

    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;

    let dataMsg = {
        dt1:dataini,
        dt2:datafim
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(dataMsg),
        headers:{
            'Content-type':'application/json'
        }
    }

    fetch("http://localhost:8080/evento/data",cabecalho) 
        .then(res => res.json() )    // extrai os dados do retorno
        .then(result => preencheEventos(result))
}

function preencheEventos(result) {
    let tabela = '<table  class="table table-sm"> <tr> <th>Data</th> <th>Alarme</th> <th>Equipamento</th> </tr> ';

    for (let i = 0; i < result.length; i++) {
       tabela = tabela + `<tr> 
                            <td>${new Date(result[i].dataevt).toLocaleString("pt-BR")} </td>
                            <td>${result[i].alarme.nome} </td>
                            <td>${result[i].equipamento.hostname} </td>
                          </tr>`          
    }

    tabela = tabela + '</table>';
    document.getElementById("tabela").innerHTML = tabela;

    let botao = `<button onclick="exportTableToCSV('Eventos.csv')">Export CSV File</button>
                <input type="button" name="imprimir" value="Imprimir" onclick="CriaPDF();">`;
             /*  <input type="button" name="imprimir" value="Imprimir" onclick="window.print();">`;*/
    document.getElementById("btn_export").innerHTML = botao;
}

function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        csv.push(row.join(","));        
    }
    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}

function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Hide download link
    downloadLink.style.display = "none";
    // Add the link to DOM
    document.body.appendChild(downloadLink);
    // Click download link
    downloadLink.click();
}

function CriaPDF() {
    var minhaTabela = document.getElementById("tabela").innerHTML;
    var style = "<style>";
    style = style + "table {width: 100%;font: 20px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";
    // CRIA UM OBJETO WINDOW
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title h2>Relatório de Eventos por periodo</title>');   // <title> CABEÇALHO DO PDF.
    win.document.write(style);                                     // INCLUI UM ESTILO NA TAB HEAD
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(minhaTabela);                          // O CONTEUDO DA TABELA DENTRO DA TAG BODY
    win.document.write('</body></html>');
    win.document.close(); 	                                         // FECHA A JANELA
    win.print();   
    win.document.close();                                                         // IMPRIME O CONTEUDO
}

