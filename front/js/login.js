function autenticar(event){
    event.preventDefault();   // evita o comportamento padrão do form. não envia os dados do form.

    let txtUsuario = document.getElementById("txtUser");
    let txtSenha = document.getElementById("txtSenha");

    let loginMsg = {
        email: txtUsuario.value,
        racf: txtUsuario.value,
        senha: txtSenha.value
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(loginMsg),
        headers:{
            'Content-type':'application/json'
        }
    }

    fetch("http://localhost:8080/login",cabecalho)   // envia para o backend a mensagem para login
        .then( res => tratarResposta(res) );  // quando o fetch receber a resposta 'res' continua enviando para a funcao.

}

function tratarResposta(resposta) {
    if (resposta.status == 200) {
       resposta.json().then( res => fazerLogin(res) ); 
    } else {
        document.getElementById("msgError").innerHTML = "Usuario/Senha inválido(a)";
    }
}

function fazerLogin(user) {
    localStorage.setItem("userLogged",JSON.stringify(user));  
    window.location = "dashmenu.html";

}